---
title: エスコフィエ『料理の手引き』全注解
author:
- 五 島　学（責任編集・訳・注釈）
- 河井 健司（訳・注釈）
- 春野 裕征（訳）
- 山 本　学（訳）
- 高 橋　昇（校正）
pandoc-latex-environment:
    main: [main]
    recette: [recette]
    frsubenv: [frsubenv]
    frsecenv: [frsecenv]
    frsecbenv: [frsecbenv]
    frchapenv: [frchapenv]
...


## 淡水魚

<div class="frsecenv">Série des Poissons d'eau douce</div>


### 典型的なマトロット

<div class="frsubenv">Matelotes types</div>

（仕上がり約10人分、魚の重量は単一種あるいは複数種で1.5 kg）

本書ではマトロットは別枠として、淡水魚の冒頭で扱うこととした。マトロットに用いる魚のほとんどはおなじ調理法になるからだ。
